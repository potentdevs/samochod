package Michal;

import Michal.kierowca.Kierowca;
import Michal.mechanik.Mechanik;
import Michal.samochod.Samochod;

public class App {
    public static void main(String[] args){
        Samochod car1 = new Samochod(30,true);
        Samochod car2 = new Samochod(9,false);
        Kierowca driver = new Kierowca();
        Mechanik mechanic = new Mechanik();
        if (driver.getCanIDrive(car1)) System.out.println("Mozesz jechac.");
        else
            mechanic.fix(car1);
        if (driver.getCanIDrive(car2)) System.out.println("Mozesz jechac.");
        else {
            mechanic.fix(car2);
        }
    }
}
